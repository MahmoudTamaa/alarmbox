FROM python:3.10.6-slim-bullseye

# RUN git clone https://gitlab.com/MahmoudTamaa/alarm-box.git

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY config.py config.py

COPY serverthreading.py serverthreading.py

CMD ["python3","-u", "serverthreading.py"]